import { Component } from "react";
import apiProduct from '../asset/ProductData.json'
import { Row, Col, Card, Button } from "react-bootstrap";

const allProduct = apiProduct.Products
console.log(allProduct)

//Giới hạn sô lượng product
const limitProduct = allProduct.slice(0, 9)

class ContentCpmponent extends Component{
    render() {
        return(
            <div className="container mt-4 mb-4">
                <h3>Product List</h3>
                <p>Show 1 - {limitProduct.length} of {allProduct.length} products</p>
                <div className="show-product">
                <Row xs={1} md={3} className="g-5 pb-3">
                    {limitProduct.map((Products) => (
                        <Col>
                        <Card>
                            <Card.Header>
                                <h5 className="text-center text-primary">{Products.Title}</h5>
                            </Card.Header>
                            <Card.Body className="bg-light text">
                                <div className="text-center">
                                    <img src={Products.ImageUrl} className="img-fluid" alt={Products.Contents} style={{height:'150px'}}></img>
                                </div>
                            <p className="text-truncate">{Products.Description}</p>
                            <p><span className="fw-bold">Category:</span> {Products.Category}</p>
                            <p><span className="fw-bold">Made by:</span> {Products.Manufacturer}</p>
                            <p><span className="fw-bold">Organic:</span> {Products.Organic? 'true':'false'}</p>
                            <p><span className="fw-bold">Price:</span> ${Products.OrgPrice}</p>
                            <div className="text-center">
                                <Button variant="primary">Add to Cart</Button>
                            </div>
                            </Card.Body>
                        </Card>
                        </Col>
                    ))}
                </Row>
                </div>
            </div>
        )
    }
}
export default ContentCpmponent;