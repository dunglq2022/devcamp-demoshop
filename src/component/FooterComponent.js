import { Component } from "react";
import { Col, Container, Row, ListGroup, NavLink } from "react-bootstrap";
import '../App.css'

class FooterComponent extends Component{
    render(){
        return (
            <footer style={{backgroundColor:'#2d3246'}} className="text-light">
                    <Container className="container" style={{width: '1000px'}}>
                        <Row className="justify-content-md-center">
                            <Col xs={4}>
                                <p className="pt-4">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <p>
                                ©  2018 . All Rights Reserved.
                                </p>
                            </Col>
                            <Col xs={4}>
                            <h5 className="pt-4">Contract</h5> 
                            <ListGroup>
                                <div className="contact-list">
                                    <div className="fw-bold">Address:</div>
                                    Kolkata, West Bengal, India
                                </div>

                                <div className="contact-list pt-3">
                                    <div className="fw-bold">email:</div>
                                    <NavLink className="link-text" href="mailto:#">info@example.com</NavLink>
                                </div>

                                <div className="contact-list pt-3">
                                    <div className="fw-bold">phone:</div>
                                    <NavLink href="telto:#"><span className="link-text">+91 99999999</span> or <span className="link-text">+91 11111111</span></NavLink>
                                </div>
                            </ListGroup>     
                            </Col>
                            <Col xs={3}>
                            <h5 className="pt-4">Links</h5>
                            <p>About</p>
                            <p>Projects</p>
                            <p>Blog</p>
                            <p>Contacts</p>
                            <p>Pricing</p>
                            </Col>
                        </Row>
                    </Container>
                    <Row className="text-center" style={{backgroundColor: '#2d3246', color: 'white', fontWeight: 'bolder'}}>
                        <Col style={{border:'1px solid hsla(0,0%,100%,.1)'}} className="pt-3">
                            <p>FACEBOOK</p>
                        </Col>
                        <Col style={{border:'1px solid hsla(0,0%,100%,.1)'}} className="pt-3">
                            <p>INSTAGRAM</p>
                        </Col>
                        <Col style={{border:'1px solid hsla(0,0%,100%,.1)'}} className="pt-3">
                            <p>TWITTER</p>
                        </Col>
                        <Col style={{border:'1px solid hsla(0,0%,100%,.1)'}} className="pt-3">
                            <p>GOOGLE</p>
                        </Col>
                    </Row>
            </footer>
        )
    }
}

export default FooterComponent;