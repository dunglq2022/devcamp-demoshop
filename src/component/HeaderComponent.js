import { Component } from "react";
import imgLogo from '../img/React-icon.svg.png'
import MenuComponent from "./MenuComponent";

class HeaderComponent extends Component {
    render(){
        return(
            <>
            <div className="header">
                <div className="container text-center">
                    <div className="logo">
                        <a href="/">
                            <img src={imgLogo} alt="logo" width={50}></img>
                        </a>
                    </div>
                    <h1>React Store</h1>
                    <p>Demo App Shop24h v1.0</p>
                </div>
                <MenuComponent/>
            </div>
            </>
        )
    }
}
export default HeaderComponent;