import { Component } from "react";

class MenuComponent extends Component{
    render(){
        return (
            <>
            <nav className="navbar navbar-expand-md " style={{backgroundColor:'#343a40'}}>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar"><span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="collapsibleNavbar">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item ms-3">
                            <a style={{color: 'white'}} aria-current="page" className="nav-link active" href="/">Home</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </>
        )
    }
}

export default MenuComponent;