
import './App.css';
import HeaderComponent from './component/HeaderComponent';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import FooterComponent from './component/FooterComponent';
import ContentCpmponent from './component/ContentComponent';

function App() {
  return (
    <div>
      <HeaderComponent/>
      <ContentCpmponent/>
      <FooterComponent/>
    </div>
  );
}

export default App;
